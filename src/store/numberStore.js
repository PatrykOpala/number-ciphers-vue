import { defineStore } from "pinia";
import { reactive, ref } from "vue";

export const useNumberCiphers = defineStore("NumberCiphers", () => {
    const score = ref(0);
    const levels = ref({
        space: {
            words: [
                {
                    mathematicalActivities: ["2+3", "5+3", "5+5"],
                    sums: [5, 8, 10],
                    word: "dom"
                }
            ]
        },
        medium: {
            words: [
                {
                    mathematicalActivities: [],
                    sums: [],
                    word: ""
                }
            ]
        },
        advanced: {
            words: [
                {
                    mathematicalActivities: [],
                    sums: [],
                    word: ""
                }
            ]
        },
        extreme: {
            words: [
                {
                    mathematicalActivities: [],
                    sums: [],
                    word: ""
                }
            ]
        }
    });
    function getScore(){
        return score.value
    }
    function getLevels(){
        return levels.value
    }
    function incrementScore(){
        score.value++
    }

    return {score, levels, getScore, getLevels, incrementScore}
});